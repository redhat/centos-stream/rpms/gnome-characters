#!/bin/bash
. /usr/share/beakerlib/beakerlib.sh || exit 1

NAME=gnome-characters

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm gnome-characters
	rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
	rlRun "cp gnome_characters_test.py $tmp"
        rlRun "pushd $tmp"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun -t -s "env LD_LIBRARY_PATH=/usr/lib64/org.gnome.Characters GI_TYPELIB_PATH=/usr/lib/girepository-1.0:/usr/lib64/org.gnome.Characters/girepository-1.0 ./gnome_characters_test.py -v" 0 "Running test cases"
        echo "==== START of `cat ${rlRun_LOG}`: log of ${test_file} ===="
        cat ${rlRun_LOG}
        echo "==== END of `cat ${rlRun_LOG}`: log of ${test_file} ===="
        rlAssertNotGrep FAIL ${rlRun_LOG}
        rlGetTestState
        rlLog "Number of failed asserts so far: ${ECODE}"
        rlFileSubmit ${rlRun_LOG}
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
