#!/usr/bin/python3

import unittest
from gi import require_version
require_version('Gc', '1.0')
from gi.repository import Gc


class TestGnomeCharacters(unittest.TestCase):

    def test_dummy(self):
        self.assertEqual(True, True)

    @unittest.expectedFailure
    def test_expected_failure(self):
        self.assertEqual('BLACK SMILING FACE', Gc.character_name("☺"))

    def test_character_name(self):
        self.assertEqual('WHITE SMILING FACE', Gc.character_name("☺"))

if __name__ == "__main__":
    unittest.main()
